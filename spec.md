upstream:
iv|[req_head|data...]

req_head:
interfered|hostlen|host|port|mac
data (with interference type 1):
req_session(random bytes)|req_number(same req has same number)|req_counter(the times)|req_len|req_content

downstream:
iv|[data...]
(with interference):
date_len|data...

interference type 1:
POST /kcsapi/.*
