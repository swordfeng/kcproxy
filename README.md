KCProxy
===
KanColle proxy

Strong enhancement for connections between your PC and server!

Usage:
1. move `config.py.samp` to `config.py` and modify the content
2. install `pycrypto`
3. `python3 server.py` on server and `python3 client.py` on client
4. game start!
